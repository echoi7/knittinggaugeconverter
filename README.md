# KnittingGaugeConverter

An application to convert knitting gauges from a pattern's given gauge to the user's gauge.

## Project Status

This project's main features are complete. Users can put in a pattern's gauge and their own gauge to calculate the pattern measurements. They can also switch between measurement systems. 

## Project Screen Shot(s)

![Knitting Gauge Converter title page](./img/screenshot.png)

## Reflection

This was a personal project to brush up on my HTML, CSS, and Javascript knowledge. I wanted to incorporate knitting into it as it was my main hobby at the time. I know that many knitters, including myself, run into the issue of converting the pattern's given gauge to their own. The numbers get tangled up in the process which leads to chaos and confusion. I wanted to create an application where knitters can calculate and keep track of their conversions.

An unexpected challenge from the start was designing the application as that is not my area of expertise. I took some time to research tools that helped me figure out the right colors and shades that stick to accessibility guidelines. 

Another struggle I had was styling the measurements system toggle button. I realized I had a long way to go to fully understand animations in CSS.

Overall, Knitting Gauge Converter was a fun project to work on while uncovering my gaps in knowledge and room for improvement. 